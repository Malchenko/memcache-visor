<?php

namespace app\controllers;

use app\models\MemcachedConfig;
use app\models\MemcachedVisor;
use Yii;

/**
 * Class MemcachedController
 *
 * Основной контроллер
 *
 * @package app\controllers
 */
class MemcachedController extends \yii\web\Controller {

	/**
	 * Вспомогательнвй метод возвращаюший объект MemcachedVisor
	 *
	 * @return MemcachedVisor
	 */
	private function getMemcached() {
		$memcachedConfig = new MemcachedConfig();
		$memcachedConfig->readConfig();
		$cfg = $memcachedConfig->getPreparedConfig();
		$memcached = new MemcachedVisor($cfg);
		return $memcached;
	}

	/**
	 * Метод контроллера ведущий на страницу с настройками
	 *
	 * @return string
	 */
	public function actionManager() {

		$memcachedConfig = new MemcachedConfig();

		if ($memcachedConfig->load(Yii::$app->request->post())) {
			$memcachedConfig->saveConfig();
		} else {
			$memcachedConfig->readConfig();
		}

		return $this->render('manager', [
			'model' => $memcachedConfig,
		]);
	}

	/**
	 * Метод контроллера ведущий на страницу с метриками
	 *
	 * @return string
	 */
	public function actionVisor() {

		$memcached = $this->getMemcached();
		$stats = $memcached->getStats();

		return $this->render('visor', [
			'servers' => $stats,
		]);
	}

	/**
	 * Метод контроллера записываюший 100 ключей с значением
	 * html кода сайта https://habrahabr.ru/
	 *
	 * @return string
	 */
	public function actionGenerate() {

		$memcached = $this->getMemcached();

		$text = file_get_contents('https://habrahabr.ru/');
		$i = 1;
		while ($i <= 100) {
			$i++;
			$key = 'key' . rand(1, 10000) . substr(md5(rand(1, 10000)), 2, 7);
			$memcached->set($key, $text, 600);
		}

		return $this->redirect('visor');
	}

	/**
	 * Метод контроллера устанавливаюший значение по ключу
	 *
	 * @return string
	 */
	public function actionSetValue() {

		$key = Yii::$app->request->post('key');
		$value = Yii::$app->request->post('value');

		$memcached = $this->getMemcached();
		$memcached->set($key, $value, 600);

		echo json_encode(sprintf('SET [key=%s, value=%s] OK', $key, $value));
		exit;
	}

	/**
	 * Метод контроллера получаюший значение по ключу
	 *
	 * @return string
	 */
	public function actionGetValue() {

		$key = Yii::$app->request->post('key');

		$memcached = $this->getMemcached();
		$value = $memcached->get($key);

		echo json_encode($value);
		exit;
	}

}
