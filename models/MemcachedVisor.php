<?php

namespace app\models;

use yii\base\Model;


/**
 * Class MemcachedVisor
 *
 * Класс обертка над Memcached
 *
 * @package app\models
 */
class MemcachedVisor extends Model {

	public static $instance = null;

	/**
	 * MemcachedVisor constructor.
	 * @param array $config
	 */
	public function __construct($config) {

		self::$instance = new \Memcached();
		if (is_array($config) && isset($config[0]['ip'], $config[0]['port'])) {
			foreach ($config as $server) {
				self::$instance->addServer($server['ip'], intval($server['port']));
			}
		}
	}

	/**
	 * Метод возвращает статистику по серверам
	 * @return array
	 */
	public function getStats() {
		return self::$instance->getStats();
	}

	/**
	 * Метод устанавливает значение в мемкеш по ключу $key
	 *
	 * @param $key
	 * @param $value
	 * @param $ttl
	 * @return bool
	 */
	public function set($key, $value, $ttl) {
		return self::$instance->set($key, $value, $ttl);
	}

	/**
	 * Метод получает значение из кеша по $key
	 * @param $key
	 * @return mixed
	 */
	public function get($key) {
		return self::$instance->get($key);
	}
}