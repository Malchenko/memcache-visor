<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use miloschuman\highcharts\Highcharts;
use yii\helpers\Url;
use yii\web\JsExpression;

?>
<h1>memcached/visor</h1>

<div class="panel-group" id="accordion">
    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">Сontrol panel</h4>
        </div>
        <div class="panel-body">
            <div class="col-lg-4">
				<?php
				echo Html::a('Generate 100 random keys', ['memcached/generate'], ['class' => 'btn btn-info btn-group-justified']);
				echo Html::tag('br');
				echo Html::a('Refresh', ['/'], ['class' => 'btn btn-info  btn-group-justified']);
				?>
            </div>

            <div class="col-lg-4">
                <div class="input-group">
					<?= Html::input('textInput', 'key', 'key', ['id' => 'input-key', 'class' => 'form-control']); ?>
					<?= Html::input('textInput', 'value', 'value', ['id' => 'input-value', 'class' => 'form-control']); ?>
                    <span class="input-group-btn"><?= Html::button('set', ['class' => 'btn btn-info', 'id' => 'set-button']); ?></span>
                </div>
            </div>

            <div class="col-lg-4">
                <div class="input-group">
					<?= Html::input('textInput', 'getkey', 'key', ['id' => 'input-get', 'class' => 'form-control']); ?>
					<?= Html::input('textInput', 'val', '', ['id' => 'output-val', 'class' => 'form-control', 'disabled' => 'disabled']); ?>
                    <span class="input-group-btn"><?= Html::button('get', ['class' => 'btn btn-info', 'id' => 'get-button']); ?></span>
                </div>
            </div>

        </div>
    </div>
	<?php
	$id = 0;
	/** @var array $servers */
	foreach ($servers as $server => $stats):
		$id++;
		?>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h4 class="panel-title">
                    Server <?= $server; ?>
                </h4>
            </div>
            <div class="panel-body">
                <div class="container col-lg-5">
					<?php
					echo Html::ul($stats, ['item' => function ($value, $key) {
						$row = Html::a(Html::tag('span', $value, ['class' => 'label label-success pull-right']) . $key, '#');
						return Html::tag(
							'li',
							$row
						);
					}, 'class' => 'nav nav-pills nav-stacked']);
					?>
                </div>
                <div class="container col-lg-6" style="height: 500px">
					<?php
					echo Highcharts::widget([
						'scripts' => [
							'modules/exporting',
							'themes/grid-light',
						],
						'options' => [
							'title' => [
								'text' => 'Memcached charts',
							],
							'chart' => [
								'height' => 1000,
							],
							'series' => [
								[
									'type' => 'pie',
									'name' => 'Used RAM',
									'data' => [
										[
											'name' => 'Used, Kb',
											'y' => intval($stats['bytes'] / 1024),
											'color' => new JsExpression('Highcharts.getOptions().colors[0]'),
										],
										[
											'name' => 'Free, Kb',
											'y' => (intval($stats['limit_maxbytes'] / 1024) - intval($stats['bytes'] / 1024)),
											'color' => new JsExpression('Highcharts.getOptions().colors[1]'),
										],
									],
									'center' => [250, 125],
									'size' => 200,
									'showInLegend' => false,
									'dataLabels' => [
										'enabled' => true,
									],
								],
								[
									'type' => 'pie',
									'name' => 'total hits / total misses',
									'data' => [
										[
											'name' => 'HITS',
											'y' => intval($stats['get_hits']),
											'color' => new JsExpression('Highcharts.getOptions().colors[2]'),
										],
										[
											'name' => 'MISSES',
											'y' => intval($stats['get_misses']),
											'color' => new JsExpression('Highcharts.getOptions().colors[3]'),
										],
									],
									'center' => [250, 425],
									'size' => 200,
									'showInLegend' => false,
									'dataLabels' => [
										'enabled' => true,
									],
								],
								[
									'type' => 'pie',
									'name' => 'traffic',
									'data' => [
										[
											'name' => 'read, Kb',
											'y' => intval($stats['bytes_read'] / 1024),
											'color' => new JsExpression('Highcharts.getOptions().colors[9]'),
										],
										[
											'name' => 'write, Kb',
											'y' => intval($stats['bytes_written'] / 1024),
											'color' => new JsExpression('Highcharts.getOptions().colors[8]'),
										],
									],
									'center' => [250, 725],
									'size' => 200,
									'showInLegend' => false,
									'dataLabels' => [
										'enabled' => true,
									],
								],
							],
						]
					]);
					?>
                </div>
            </div>
        </div>
		<?php
	endforeach;
	?>
</div>
<script>
    $(function () {

        $('#set-button').click(function () {
            var setKey = $('#input-key').val();
            var setValue = $('#input-value').val();

            $.ajax({
                    dataType: 'json',
                    url: '<?= Url::toRoute(['/memcached/set-value'])?>',
                    type: 'POST',
                    data: {key: setKey, value: setValue}
                }
            ).fail(function () {
                    alert('Произошла ошибка при проверки сервера');
                }
            ).success(function (response) {
                    alert(response);
                }
            );
        });


        $('#get-button').click(function () {
            var getKey = $('#input-get').val();

            $.ajax({
                    dataType: 'json',
                    url: '<?= Url::toRoute(['/memcached/get-value'])?>',
                    type: 'POST',
                    data: {key: getKey}
                }
            ).fail(function () {
                    alert('Произошла ошибка при проверки сервера');
                }
            ).success(function (response) {
                    $('#output-val').val(response);
                }
            );
        });

    });
</script>