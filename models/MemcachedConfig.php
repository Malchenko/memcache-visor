<?php

namespace app\models;

use yii\base\Model;

/**
 * Class MemcachedConfig
 *
 * Класс для работы с текстовым конфигом мемкеша
 *
 * @package app\models
 */
class MemcachedConfig extends Model {

	const CONFIG_FILE = 'memcached.txt';
	public $config;

	/**
	 * @return array the validation rules.
	 */
	public function rules() {
		return [
			['config', 'required']
		];
	}

	/**
	 * Чтение конфига из файла
	 *
	 * @return bool успешность чтения конфига
	 */
	public function readConfig() {
		$this->config = 'File with config not exist';
		if (!file_exists(self::CONFIG_FILE)) {
			return false;
		}

		try {
			$this->config = file_get_contents(self::CONFIG_FILE);
		} catch (\Exception $e) {
			$this->config = 'Exception on read config, ' . $e->getMessage();
			return false;
		}
		return true;
	}

	/**
	 * Сохранение конфига в файл
	 *
	 * @return bool успешность чтения конфига
	 */
	public function saveConfig() {
		try {
			file_put_contents(self::CONFIG_FILE, $this->config);
		} catch (\Exception $e) {
			$this->config = 'Exception on save config, ' . $e->getMessage();
			return false;
		}
		return true;
	}

	/**
	 * Метод подготавливает текстовый конфиг к нужному формату
	 *
	 * @return array
	 */
	public function getPreparedConfig() {
		$result = [];
		$rows = explode("\n", $this->config);
		foreach ($rows as $row) {
			list($ip, $port) = explode(':', $row);
			$result[] = [
				'ip' => $ip,
				'port' => $port,
			];
		}
		return $result;
	}
}