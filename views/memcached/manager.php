<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\widgets\ActiveForm;

?>
    <h1>memcached/manager</h1>

    <div class="panel panel-primary">
        <div class="panel-heading">Config example</div>
        <div class="panel-body">
            10.252.17.207:11211<br>
            10.252.17.196:11211
        </div>
    </div>

<?php $form = ActiveForm::begin(); ?>
<?= $form->field($model, 'config')->textarea(['rows' => '6']) ?>
<?= Html::submitButton('save', ['class' => 'btn btn-success']) ?>
<?php ActiveForm::end(); ?>